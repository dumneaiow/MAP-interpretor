package controller;

import exception.ControllerException;
import javafx.util.Pair;
import model.FileITable;
import model.IHeap;
import model.PrgState;
import model.container.FileTable;
import repository.Repository;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class Controller {
    private Repository repository;
    private ExecutorService executor;

    public Controller(Repository repository) {
        this.repository = repository;
    }

    public Controller() {
    }

    private List<PrgState> removeCompletedPrg(List<PrgState> list) {
        return list.stream()
                .filter(PrgState::isNotCompleted)
                .collect(Collectors.toList());
    }

    private void oneStepForAllPrg(List<PrgState> prgList) throws ControllerException {
//        prgList.forEach(prg -> {
//            try {
//                repository.logProgStateExe(prg);
//            } catch (RepositoryException e) {
//            }
//        });
        List<Callable<PrgState>> callList = prgList.stream()
                .map((PrgState p) -> (Callable<PrgState>) (p::oneStep))
                .collect(Collectors.toList());
        try {
            List<PrgState> newPrgList = executor.invokeAll(callList)
                    .stream()
                    .map(future -> {
                        try {
                            return future.get();
                        } catch (InterruptedException | ExecutionException e) {
                            throw new ControllerException("Execution stopped");
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            prgList.addAll(newPrgList);
        } catch (InterruptedException e) {
            throw new ControllerException("interruptedException");
        }
        prgList.forEach(repository::logProgStateExe);
        repository.setPrgList(prgList);
    }

    private Map<Integer, Integer> conservativeGarbageCollector(List<PrgState> list) {
        return list.get(0).getHeap().getContent().entrySet().stream()
                .filter(e -> {
                            for (PrgState prgState : list) {
                                if (prgState.getSymbolTable().getValues().contains(e.getKey()))
                                    return true;
                            }
                            return false;
                        }
                )
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

    }

    private void closeAllFiles(Collection<Pair<String, BufferedReader>> readers) {
        readers.stream().map(Pair::getValue).forEach(e -> {
            try {
                if (e != null && e.ready()) e.close();
            } catch (IOException ex) {
            }
        });
    }

    public void allSteps() throws ControllerException {
        List<PrgState> prgList = removeCompletedPrg(repository.getPrgList());
        if ( prgList.size() == 0)
        {
            throw new ControllerException("Nothing to execute");
        }
        executor = Executors.newFixedThreadPool(2);
        FileITable<Integer,Pair<String,BufferedReader>> fileITable = prgList.get(0).getFileTable();
        while (prgList.size() > 0) {
            IHeap<Integer, Integer> heap = prgList.get(0).getHeap();
            heap.setContent(conservativeGarbageCollector(prgList));
            oneStepForAllPrg(prgList);
            prgList = removeCompletedPrg(prgList);
        }
        executor.shutdownNow();
        closeAllFiles(fileITable.getValues());
        repository.setPrgList(prgList);
    }
}
