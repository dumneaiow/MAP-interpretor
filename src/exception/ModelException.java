package exception;

public class ModelException extends RuntimeException{
    public ModelException(String message) {
        super(message);
    }
}
