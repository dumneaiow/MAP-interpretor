package model.container;

import model.IHeap;

import java.util.HashMap;
import java.util.Map;

public class Heap<V> implements IHeap<Integer,V> {
    private Map<Integer,V> map = new HashMap<>();
    private int nextFree = 1;


    @Override
    public Integer put( V value) {
        map.put(nextFree,value);
        return nextFree++;
    }

    @Override
    public Map<Integer, V> getContent() {
        return map;
    }

    @Override
    public void setContent(Map<Integer, V> newContent) {
        map = newContent;
    }

    @Override
    public void update(Integer key, V value) {
        map.put(key,value);
    }

    @Override
    public V get(Integer key) {
        return map.get(key);
    }

    @Override
    public boolean contains(Integer key) {
        return map.containsKey(key);
    }

    @Override
    public Iterable<Integer> getKeys() {
        return map.keySet();
    }

    @Override
    public Iterable<V> getValues() {
        return map.values();
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<Integer,V> e : map.entrySet()){
            sb.append('\n');
            sb.append(e.getKey());
            sb.append("-->");
            sb.append(e.getValue());
        }
        return sb.toString();
    }

}
