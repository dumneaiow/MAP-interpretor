package model.container;

import exception.StackModelException;
import model.MyIStack;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Stack;

public class MyStack<T> implements MyIStack<T> {
    private Deque<T> stack;

    public MyStack() {
        this.stack = new ArrayDeque<>();
    }

    @Override
    public T pop() throws StackModelException {
        if (stack.isEmpty())
            throw new StackModelException("Stack: Stack is empty");
        return stack.pop();
    }

    @Override
    public boolean isEmpty() {
        return stack.isEmpty();
    }

    @Override
    public void push(T element) {
        stack.push(element);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (T t : stack){
            sb.append('\n');
            sb.append(t);
        }
        return sb.toString();
    }

    @Override
    public Iterable<T> getAll() {
        return stack;
    }
}
