package model.expression;

import exception.ModelException;
import model.Expression;
import model.IHeap;
import model.MyIMap;

public class Less implements Expression {
    private Expression left,right;

    public Less(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Integer eval(MyIMap<String, Integer> symbolTable, IHeap<Integer, Integer> heap) throws ModelException {
        if (left.eval(symbolTable, heap) < right.eval(symbolTable, heap))
            return 1;
        return 0;
    }
    @Override
    public String toString() {
        return left+" < "+right;

    }
}
