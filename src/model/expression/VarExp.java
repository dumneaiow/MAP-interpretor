package model.expression;

import exception.MapModelException;
import model.Expression;
import model.IHeap;
import model.MyIMap;

public class VarExp implements Expression {
    private String varName;

    public VarExp(String varName) {
        this.varName = varName;
    }

    @Override
    public Integer eval(MyIMap<String, Integer> symbolTable, IHeap<Integer,Integer> heap) throws MapModelException {
        return symbolTable.get(varName);
    }

    @Override
    public String toString() {
        return "" + varName;
    }
}
