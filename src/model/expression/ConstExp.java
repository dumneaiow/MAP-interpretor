package model.expression;

import model.Expression;
import model.IHeap;
import model.MyIMap;

import java.util.Map;

public class ConstExp implements Expression {
    private int val;

    public ConstExp(int val) {
        this.val = val;
    }

    @Override
    public Integer eval(MyIMap<String, Integer> SymbolTable, IHeap<Integer,Integer> heap) {
        return val;
    }

    @Override
    public String toString() {
        return ""+val;
    }

}
