package model.expression;

import exception.ModelException;
import model.Expression;
import model.IHeap;
import model.MyIMap;

public class AddM10Exp implements Expression {
    private Expression left, right;

    public AddM10Exp(Expression left, Expression right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public Integer eval(MyIMap<String, Integer> symbolTable, IHeap<Integer, Integer> heap) throws ModelException {
        return (this.left.eval(symbolTable, heap) + this.right.eval(symbolTable, heap)) % 10;
    }

    @Override
    public String toString(){
        return "("+left+" + "+right+") MOD 10";
    }
}
