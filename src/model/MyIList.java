package model;

import exception.ListModelException;

public interface MyIList<T> {
    void add(T element);
    Iterable<T> getAll();
}
