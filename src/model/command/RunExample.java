package model.command;

import controller.Controller;
import exception.ControllerException;

public class RunExample extends Command {
    Controller controller;

    public RunExample(String key, String description, Controller controller) {
        super(key, description);
        this.controller = controller;
    }

    @Override
    public void execute() {
        try{
            controller.allSteps();
        }
        catch(ControllerException e){
            System.out.println(e.getMessage());
        }
        catch(RuntimeException e){
            System.out.println("Something happened: "+e.getMessage());
        }
    }
}
