package model;

import exception.ModelException;

public interface Stmt {
    public PrgState execute(PrgState prgState) throws ModelException;
}
