package model.statement;

import exception.ModelException;
import model.Expression;
import model.PrgState;
import model.Stmt;

import java.io.IOException;

public class ReadFile implements Stmt {

    private Expression exp_file_id;
    private String var_name;

    public ReadFile(Expression exp_file_id, String var_name) {
        this.exp_file_id = exp_file_id;
        this.var_name = var_name;
    }

    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        String line;
        try{
            line = prgState.getFileTable().find(exp_file_id.eval(prgState.getSymbolTable(),prgState.getHeap())).getValue().readLine();
            if (line==null){
                prgState.getSymbolTable().put(var_name,0);
                return null;
            }
            prgState.getSymbolTable().put(var_name,Integer.parseInt(line));
            return null;
        }
        catch (IOException|NullPointerException e){
            throw new RuntimeException();
        }
    }

    @Override
    public String toString() {
        return "readFile("+ exp_file_id + "," + var_name + ')';
    }
}
