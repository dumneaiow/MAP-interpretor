package model.statement;

import exception.ModelException;
import model.Expression;
import model.PrgState;
import model.Stmt;

public class IfStmt implements Stmt {
    private Expression expr;
    private Stmt expresion1;
    private Stmt expresion2;

    public IfStmt(Expression expr, Stmt expresion1, Stmt expresion2) {
        this.expr = expr;
        this.expresion1 = expresion1;
        this.expresion2 = expresion2;
    }

    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        if (expr.eval(prgState.getSymbolTable(),prgState.getHeap())==0)
            prgState.getExecStack().push(expresion2);
        else
            prgState.getExecStack().push(expresion1);
        return null;
    }

    @Override
    public String toString() {
        return "If(" + expr +")then{"+ expresion1 +
                "}else{" + expresion2 +
                "}";
    }
}
