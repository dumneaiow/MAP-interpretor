package model.statement;

import model.PrgState;
import model.Stmt;

public class CompStmt implements Stmt {
    private Stmt first;
    private Stmt second;

    public CompStmt(Stmt first, Stmt second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public PrgState execute(PrgState prgState) {
        prgState.getExecStack().push(second);
        prgState.getExecStack().push(first);
        return null;
    }

    @Override
    public String toString(){
        return first+""+second;
    }
}
