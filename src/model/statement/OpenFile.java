package model.statement;

import exception.ModelException;
import javafx.util.Pair;
import model.PrgState;
import model.Stmt;
import model.util.IDGenerator;

import java.io.*;
import java.util.Map;

public class OpenFile implements Stmt {
    private String var_file_id;
    private String filename;

    public OpenFile(String var_file_id, String filename) {
        this.var_file_id = var_file_id;
        this.filename = filename;
    }


    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        if (isOpen(prgState))
            throw new RuntimeException("File is already opened");
        try {
            Pair<String,BufferedReader> entry = new Pair<>(filename,new BufferedReader(new FileReader(filename)));
            int file_descriptor = IDGenerator.generateId();
            prgState.getFileTable().add(file_descriptor,entry);
            prgState.getSymbolTable().put(var_file_id,file_descriptor);
        }
        catch (FileNotFoundException e){
            throw new ModelException("file not found");
        }
        return null;
    }

    private boolean isOpen(PrgState prgState){
        for (Pair<String,BufferedReader> e : prgState.getFileTable().getValues()){
            if (e.getKey().equals(filename))
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "openRFile("+var_file_id  + "," + filename + ')';
    }
}
