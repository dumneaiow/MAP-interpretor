package model.statement;

import exception.ModelException;
import model.Expression;
import model.PrgState;
import model.Stmt;

public class NewHeap implements Stmt {
    private String var;
    private Expression expr;

    public NewHeap(String var, Expression expr){
        this.var = var;
        this.expr = expr;
    }

    public PrgState execute(PrgState prgState) throws ModelException
    {
        int val = expr.eval(prgState.getSymbolTable(), prgState.getHeap());
        prgState.getSymbolTable().put(var, prgState.getHeap().put(val));
        return null;
    }

    @Override
    public String toString() {
        return "new(" + var +','+  expr + ");";
    }
}