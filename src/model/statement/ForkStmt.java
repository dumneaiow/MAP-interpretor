package model.statement;

import exception.ModelException;
import model.MyIMap;
import model.MyIStack;
import model.PrgState;
import model.Stmt;
import model.container.MyStack;

public class ForkStmt implements Stmt{
    private Stmt statement;

    public ForkStmt(Stmt statement) {
        this.statement = statement;
    }

    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        MyIMap<String,Integer> map = prgState.getSymbolTable().getCopy();
        MyIStack<Stmt> stack = new MyStack<>();
        stack.push(statement);
        return new PrgState(stack,map,prgState.getOutput(),prgState.getFileTable(),prgState.getHeap(),prgState.getId()*10);
    }

    @Override
    public String toString() {
        return "ForkStmt("+statement+")";
    }
}
