package model.statement;

import exception.ModelException;
import model.Expression;
import model.PrgState;
import model.Stmt;

import java.io.IOException;

public class CloseFile implements Stmt {

    private Expression exp_file_id;

    public CloseFile(Expression exp_file_id) {
        this.exp_file_id = exp_file_id;
    }

    @Override
    public String toString() {
        return "closeRFile("+exp_file_id +')';
    }

    @Override
    public PrgState execute(PrgState prgState) throws ModelException {
        try {
            int exp = exp_file_id.eval(prgState.getSymbolTable(),prgState.getHeap());
            if (prgState.getFileTable().contains(exp)) {
                prgState.getFileTable().find(exp).getValue().close();
                prgState.getFileTable().remove(exp);
            } else
                throw new RuntimeException();
        }
        catch (IOException e){
            throw new RuntimeException();
        }
        return null;
    }
}
