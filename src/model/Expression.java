package model;


import exception.ModelException;

public interface Expression {
    public Integer eval(MyIMap<String,Integer> symbolTable, IHeap<Integer,Integer> heap) throws ModelException;
}
