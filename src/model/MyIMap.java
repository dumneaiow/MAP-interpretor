package model;

import exception.MapModelException;

import java.util.Collection;
import java.util.Map;


public interface MyIMap<K,V> {
    V get(K key) throws MapModelException;
    void put(K key,V value);
    Iterable<K> getKeys();
    Collection<V> getValues();
    boolean contains(K key);
    MyIMap<K,V> getCopy();
}
